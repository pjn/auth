<?php

namespace Pjn\Auth;

/*
##############
### SENDER ###
##############

Example passing encrypted data
------------------------------

$auth = new Cipher("TOKEN", "SECRET");

$authData = $auth->encrypt([
    "email" => post('email'),
    "password" => post('password'),
]);

list($http_code, $results) = $auth->auth("http://www.auth.local/rest");


Example to show passing data that is not encrypted
--------------------------------------------------

$auth = new Cipher("TOKEN", "SECRET");

list($http_code, $results) = $auth->auth("http://www.auth.local/rest", [
    "email" => post('email'),
    "password" => post('password'),
]);

################
### RECEIVER ###
################

Handling encrypted data.
-----------------------
// Data is decoded, decrypted and available through $data variable

$auth = new Cipher("TOKEN", "SECRET");

list($success, $data) = $auth->decrypt([
    'iv' => post('iv'),
    'secret' => post('secret'),
    'email' => post('email'),
    'password' => post('password'),
]);

if (!$success)
{
    throw new Exception("Unauthorized", 401);
}


Handling not encrypted data
----------------------------

$auth = new Cipher("TOKEN", "SECRET");

list($success, $data) = $auth->decrypt([
    'iv' => post('iv'),
    'secret' => post('secret'),
]);

if (!$success)
{
    throw new Exception("Unauthorized", 401);
}

// Just returning decoded data here as an exaple.
// Any logic can be applied here to the posted data and result returned below.
$data = [
    'email' => base64_decode(post('email')),
    'password' => base64_decode(post('password')),
];

*/


class Cipher
{
    /** @var string $token */
    private $token;

    /** @var string $secret */
    private $secret;

    /** @var string $url */
    private $url;

    /** @var array $encryptedData  */
    private $encryptedData = [];


    /** @var array $data */
    private $data = [];

    private $curlopt_ssl_verifypeer = 1;
    private $curlopt_ssl_verifyhost = 2;


    public function __construct($token, $secret)
    {
        $this->token = $token;
        $this->secret = $secret;
    }

    /**
     * @return int
     */
    public function getCurloptSslVerifypeer()
    {
        return $this->curlopt_ssl_verifypeer;
    }

    /**
     * @param int $curlopt_ssl_verifypeer
     */
    public function setCurloptSslVerifypeer($curlopt_ssl_verifypeer)
    {
        $this->curlopt_ssl_verifypeer = abs(intval($curlopt_ssl_verifypeer));
    }

    /**
     * @return int
     */
    public function getCurloptSslVerifyhost()
    {
        return $this->curlopt_ssl_verifyhost;
    }

    /**
     * @param int $curlopt_ssl_verifyhost
     */
    public function setCurloptSslVerifyhost($curlopt_ssl_verifyhost)
    {
        $this->curlopt_ssl_verifyhost = abs(intval($curlopt_ssl_verifyhost));
    }



    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        foreach ($data as $k => $v)
        {
            $this->data[$k] = base64_encode($v);
        }
    }

    /**
     * @return array
     */
    public function getEncryptedData()
    {
        if (!$this->encryptedData)
        {
            $this->encrypt();
        }
        return $this->encryptedData;
    }

    /**
     * @param array $encryptedData
     */
    public function setEncryptedData(array $encryptedData)
    {
        foreach ($encryptedData as $k => $v)
        {
            $this->encryptedData[$k] = base64_encode($v);
        }
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Generate URL-encoded query string from merged data and secret.
     * @return string
     */
    private function getPostfields()
    {
        $data = array_merge($this->getData(), $this->getEncryptedData());
        $data =  http_build_query($data);
        return $data;
    }


    /**
     * Runs curl post request to supplied $url passing optional $data if supplied.
     * @param $url
     * @param array|null $data
     * @return array
     */
    public function auth($url, array $data=[])
    {
        $this->setUrl($url);

        $this->setData($data);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 20);
        curl_setopt($curl, CURLOPT_FAILONERROR, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $this->getPostfields());


        // Just to get the curl working on localhost, change to use ssl for go live.
        // Watch out that you're not sending any sensitive data; turning off SSL verification
        // like this can leave you open to hackers
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, $this->getCurloptSslVerifypeer());
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, $this->getCurloptSslVerifyhost());

        $result = curl_exec($curl);

        $http_code =  curl_getinfo($curl, CURLINFO_HTTP_CODE);

        // curl_errno($curl);
        // curl_error($curl);

        curl_close($curl);

        return [$http_code, $result];
    }


    /**
     * Encrypts data for transaction.
     * @param $data array
     * @return null
     */
    public function encrypt(array $data=[])
    {
        $key = md5($this->token);
        $m = mcrypt_module_open('rijndael-256', '', 'cbc', '');
        $iv = mcrypt_create_iv((mcrypt_enc_get_iv_size($m)), MCRYPT_DEV_URANDOM);
        mcrypt_generic_init($m, $key, $iv);
        $secret = mcrypt_generic($m, $this->secret);

        if ($data)
        {
            foreach ($data as $k => $v)
            {
                $this->setEncryptedData([$k => mcrypt_generic($m, $v)]);
            }
        }

        $this->setEncryptedData([
            'iv' => $iv,
            'secret' => $secret,
        ]);

        mcrypt_generic_deinit($m);
        mcrypt_module_close($m);
    }


    /**
     * Returns success/fail flag and data array
     * @param array $args
     * @return array
     */
    public function decrypt(array $args)
    {
        $data = [];

        if (!array_key_exists('iv', $args) || !array_key_exists('secret', $args))
        {
            return [false, $data];
        }

        $key = md5($this->token);
        $m = mcrypt_module_open('rijndael-256', '', 'cbc', '');
        $iv = base64_decode($args['iv']);
        mcrypt_generic_init($m, $key, $iv);
        $secret = trim(mdecrypt_generic($m, base64_decode($args['secret'])));

        if ($secret != $this->secret)
        {
            return [false, $data];
        }

        foreach ($args as $k => $v)
        {
            switch ($k)
            {
                case "iv":
                case "secret":
                {
                    break;
                }

                default:
                {
                    $data[$k] = trim(mdecrypt_generic($m, base64_decode($v)));
                }
            }
        }

        mcrypt_generic_deinit($m);
        mcrypt_module_close($m);

        return [true, $data];
    }

}
